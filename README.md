akka-tutorials
==============

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/pedrorijo91/akka-tutorials?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

[![Codacy Badge](https://www.codacy.com/project/badge/6b5481cb463643479e538970bc4bfb34)](https://www.codacy.com)

Akka tutorials extracted from [Alvin Alexander](http://alvinalexander.com/) :

* http://alvinalexander.com/scala/scala-akka-actors-ping-pong-simple-example

* http://alvinalexander.com/scala/simple-scala-akka-actor-examples-hello-world-actors
